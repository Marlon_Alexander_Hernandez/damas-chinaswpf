﻿Class MainWindow
    'Declaracion de la matriz de ellipses.
    Dim ellipses As New ArrayList()
    Dim namePlayers(5) As String
    Dim contadorElipses As Integer = 0
    Dim control As Object
    Dim fichas As Object
    Dim three(44) As UserControl
    Dim contador3 As Integer = 0
    Dim six(59) As UserControl
    Dim contador6 As Integer = 0


    Private Sub LoadBoard() ' <- metodo para crear el primer triangulo que conforma el tablero.

        Dim izq As Integer = 220            ']
        Dim up As Integer = 15              ']
        Dim firstCounter As Integer = 0     ']  <- Declaracion de variables y contadores a utilizar.
        Dim secondCounter As Integer = 0    ']
        Dim izq2 As Integer = 220           ']
        Dim up2 As Integer = 383            ']

        For y As Integer = 0 To 12 '<- 'Primer For. Indica el parametro del numero de filas horizontales.

            For x As Integer = 0 To firstCounter '<- segundo For. Crea cada ellipse y le asigna una determinada posicion dentro del Canvas en el eje x.
                Dim elipsito As New Ellipse
                elipsito.Width = 25
                elipsito.Height = 25
                elipsito.Fill = Brushes.White
                Canvas.SetLeft(elipsito, izq)
                Canvas.SetTop(elipsito, up)
                ellipses.Add(elipsito)
                zone.Children.Add(elipsito)
                AddHandler elipsito.MouseUp, AddressOf SeleccionarPosicion
                contadorElipses += 1
                izq += 26
            Next
            izq = izq - ((26 * (firstCounter + 1) + 13))
            up += 23
            firstCounter += 1
        Next

        For ye As Integer = 0 To 12 ' <- metodo para crear el segundo triangulo, el triangulo invertido que completa la forma de estrella.
            For ex As Integer = 0 To secondCounter
                Dim elipsito2 As New Ellipse
                elipsito2.Height = 25
                elipsito2.Width = 25
                elipsito2.Fill = Brushes.White
                Canvas.SetLeft(elipsito2, izq2)
                Canvas.SetTop(elipsito2, up2)
                ellipses.Add(elipsito2)
                zone.Children.Add(elipsito2)
                AddHandler elipsito2.MouseUp, AddressOf SeleccionarPosicion
                contadorElipses += 1
                izq2 += 26
            Next
            izq2 = izq2 - ((26 * (secondCounter + 1) + 13))
            up2 -= 23
            secondCounter += 1
        Next
        secondCounter += 0
    End Sub


    Private Sub AddGreen()  '<- metodo para agregar los controles de usuarios sobre el tablero.
        Dim contador As Integer = 0
        Dim left As Integer = 220
        Dim top As Integer = 15

        For y As Integer = 0 To 3
            For x As Integer = 0 To contador
                Dim greenOne As New greenOnes
                Canvas.SetLeft(greenOne, left)
                Canvas.SetTop(greenOne, top)
                zone.Children.Add(greenOne)
                AddHandler greenOne.MouseLeftButtonDown, AddressOf Move_MouseLeftButtonDown
                six(contador6) = greenOne
                contador6 += 1
                left += 26
            Next
            left = left - (26 * (contador + 1) + 13)
            top += 23
            contador += 1
        Next
        contador6 = 0
    End Sub

    Private Sub AddYellow()
        Dim contador As Integer = 0
        Dim left As Integer = 220
        Dim top As Integer = 383

        For y As Integer = 0 To 3
            For x As Integer = 0 To contador
                Dim yellowOne As New yellowOnes
                Canvas.SetLeft(yellowOne, left)
                Canvas.SetTop(yellowOne, top)
                zone.Children.Add(yellowOne)
                AddHandler yellowOne.MouseLeftButtonDown, AddressOf Move_MouseLeftButtonDown
                six(contador6) = yellowOne
                contador6 += 1
                left += 26
            Next
            left = left - (26 * (contador + 1) + 13)
            top -= 23
            contador += 1
        Next
    End Sub

    Private Sub AddGray()
        Dim contador As Integer = 0
        Dim left As Integer = 337
        Dim top As Integer = 176

        For y As Integer = 0 To 3
            For x As Integer = 0 To contador
                Dim grayOne As New grayOnes
                Canvas.SetLeft(grayOne, left)
                Canvas.SetTop(grayOne, top)
                zone.Children.Add(grayOne)
                AddHandler grayOne.MouseLeftButtonDown, AddressOf Move_MouseLeftButtonDown
                six(contador6) = grayOne
                contador6 += 1
                left += 26
            Next
            left = left - (26 * (contador + 1) + 13)
            top -= 23
            contador += 1
        Next
    End Sub

    Private Sub AddOrange()
        Dim contador As Integer = 0
        Dim left As Integer = 103
        Dim top As Integer = 176

        For y As Integer = 0 To 3
            For x As Integer = 0 To contador
                Dim orangeOne As New orangeOnes
                Canvas.SetLeft(orangeOne, left)
                Canvas.SetTop(orangeOne, top)
                zone.Children.Add(orangeOne)
                AddHandler orangeOne.MouseLeftButtonDown, AddressOf Move_MouseLeftButtonDown
                six(contador6) = orangeOne
                contador6 += 1
                left += 26
            Next
            left = left - (26 * (contador + 1) + 13)
            top -= 23
            contador += 1
        Next

    End Sub

    Private Sub AddBlue()
        Dim contador As Integer = 0
        Dim left As Integer = 103
        Dim top As Integer = 222

        For y As Integer = 0 To 3
            For x As Integer = 0 To contador
                Dim blueOne As New blueOnes
                Canvas.SetLeft(blueOne, left)
                Canvas.SetTop(blueOne, top)
                zone.Children.Add(blueOne)
                AddHandler blueOne.MouseLeftButtonDown, AddressOf Move_MouseLeftButtonDown
                six(contador6) = blueOne
                contador6 += 1
                left += 26
            Next
            left = left - (26 * (contador + 1) + 13)
            top += 23
            contador += 1
        Next
    End Sub

    Private Sub AddRed()
        Dim contador As Integer = 0
        Dim left As Integer = 337
        Dim top As Integer = 222

        For y As Integer = 0 To 3
            For x As Integer = 0 To contador
                Dim redOne As New redOnes
                Canvas.SetLeft(redOne, left)
                Canvas.SetTop(redOne, top)
                AddHandler redOne.MouseLeftButtonDown, AddressOf Move_MouseLeftButtonDown
                zone.Children.Add(redOne)
                six(contador3) = redOne
                contador3 += 1
                left += 26
            Next
            left = left - (26 * (contador + 1) + 13)
            top += 23
            contador += 1
        Next
    End Sub
    '***************************FIN DE LA COLOCACION DE CONTROLES DE USUARIO*****************************


    Private Sub AddGreen3() ' <- AddGreen3, AddGray3 & AddRed3 son metodos cuya funcion es agregar una fila extra cuando el mode de juego sea de 3 jugadores.
        Dim contador As Integer = 0
        Dim left As Integer = 220
        Dim top As Integer = 15

        For y As Integer = 0 To 4
            For x As Integer = 0 To contador
                Dim greenOne As New greenOnes
                Canvas.SetLeft(greenOne, left)
                Canvas.SetTop(greenOne, top)
                zone.Children.Add(greenOne)
                AddHandler greenOne.MouseLeftButtonDown, AddressOf Move_MouseLeftButtonDown
                six(contador6) = greenOne
                contador6 += 1
                left += 26
            Next
            left = left - (26 * (contador + 1) + 13)
            top += 23
            contador += 1
        Next
        contador6 = 0
    End Sub


    Private Sub AddBlue3()
        Dim contador As Integer = 0
        Dim left As Integer = 116
        Dim top As Integer = 199

        For y As Integer = 0 To 4
            For x As Integer = 0 To contador
                Dim blueOne As New blueOnes
                Canvas.SetLeft(blueOne, left)
                Canvas.SetTop(blueOne, top)
                zone.Children.Add(blueOne)
                AddHandler blueOne.MouseLeftButtonDown, AddressOf Move_MouseLeftButtonDown
                six(contador6) = blueOne
                contador6 += 1
                left += 26
            Next
            left = left - (26 * (contador + 1) + 13)
            top += 23
            contador += 1
        Next
        contador = 0
    End Sub

    Private Sub AddRed3()
        Dim contador As Integer = 0
        Dim left As Integer = 324
        Dim top As Integer = 199

        For y As Integer = 0 To 4
            For x As Integer = 0 To contador
                Dim redOne As New redOnes
                Canvas.SetLeft(redOne, left)
                Canvas.SetTop(redOne, top)
                zone.Children.Add(redOne)
                AddHandler redOne.MouseLeftButtonDown, AddressOf Move_MouseLeftButtonDown
                six(contador6) = redOne
                contador6 += 1
                left += 26
            Next
            left = left - (26 * (contador + 1) + 13)
            top += 23
            contador += 1
        Next
        contador = 0
    End Sub
    '**********************************************************************

    Private Sub Btn1_clicked() Handles Btn1.Click ' <- Funcionalidad del boton de tres jugadores.
        Reinicio()
        zone.Children.Add(labelContainer)
        AddGreen3()
        AddRed3()
        AddBlue3()
        Btn1.IsEnabled = False : Btn2.IsEnabled = False
        txtPlayer1.IsEnabled = True : txtPlayer2.IsEnabled = False
        txtPlayer3.IsEnabled = False : txtPlayer4.IsEnabled = False
        txtPlayer5.IsEnabled = True : txtPlayer6.IsEnabled = True
    End Sub

    Private Sub Btn2_clicked() Handles Btn2.Click ' <- Funcionalidad del boton de seis jugadores.
        Reinicio()
        zone.Children.Add(labelContainer)
        AddGreen()
        AddGray()
        AddRed()
        AddOrange()
        AddBlue()
        AddYellow()
        Btn1.IsEnabled = False
        Btn2.IsEnabled = False
        txtPlayer1.IsEnabled = True : txtPlayer2.IsEnabled = True
        txtPlayer3.IsEnabled = True : txtPlayer4.IsEnabled = True
        txtPlayer5.IsEnabled = True : txtPlayer6.IsEnabled = True
    End Sub

    'El método Move_MouseLeftButtonDown ejecuta el método Predictios creando asi las predicciones en los elipses vacios al momento de dar click sobre un userControl.
    Private Sub Move_MouseLeftButtonDown(sender As Object, e As MouseEventArgs)
        Predictions(sender)
    End Sub

    Private Sub Predictions(ByVal moneda As Object) '<- método que colorea los elipses en los que se le permite moverse a la ficha seleccionada.

        For index = 0 To ellipses.Count - 1

            If (Canvas.GetLeft(moneda) - 26) = (Canvas.GetLeft(ellipses(index))) And (Canvas.GetTop(moneda)) = Canvas.GetTop(ellipses(index)) Then
                ellipses(index).Fill = Brushes.CadetBlue
                SetControl(moneda)
            End If

            If (Canvas.GetLeft(moneda) + 26) = (Canvas.GetLeft(ellipses(index))) And (Canvas.GetTop(moneda)) = Canvas.GetTop(ellipses(index)) Then
                ellipses(index).Fill = Brushes.CadetBlue
                SetControl(moneda)
            End If

            If (Canvas.GetLeft(moneda) - 13) = (Canvas.GetLeft(ellipses(index))) And (Canvas.GetTop(moneda) + 23) = Canvas.GetTop(ellipses(index)) Then
                ellipses(index).fill = Brushes.CadetBlue
                SetControl(moneda)
            End If

            If (Canvas.GetLeft(moneda) + 13) = (Canvas.GetLeft(ellipses(index))) And (Canvas.GetTop(moneda) - 23) = Canvas.GetTop(ellipses(index)) Then
                ellipses(index).fill = Brushes.CadetBlue
                SetControl(moneda)
            End If

            If (Canvas.GetLeft(moneda) - 13) = (Canvas.GetLeft(ellipses(index))) And (Canvas.GetTop(moneda) - 23) = Canvas.GetTop(ellipses(index)) Then
                ellipses(index).fill = Brushes.CadetBlue
                SetControl(moneda)
            End If

            If (Canvas.GetLeft(moneda) + 13) = (Canvas.GetLeft(ellipses(index))) And (Canvas.GetTop(moneda) + 23) = Canvas.GetTop(ellipses(index)) Then
                ellipses(index).fill = Brushes.CadetBlue
                SetControl(moneda)
            End If
        Next
    End Sub

    Public Sub SetControl(ByRef sender As Object)
        control = sender
    End Sub

    Private Function GetControl()
        Return control
    End Function

    Private Sub SeleccionarPosicion(sender As Object, e As MouseEventArgs) ' <- método que selecciona la siguiente posicion de la ficha y verifica su validez
        If sender.fill.ToString = "#FF5F9EA0" Then
            Canvas.SetLeft(GetControl(), Canvas.GetLeft(sender))
            Canvas.SetTop(GetControl(), Canvas.GetTop(sender))
            ChangeColors()
        Else
            MsgBox("Movimiento invalido", MsgBoxStyle.Critical)
        End If
    End Sub

    'método para cambiar colores de los elipses desocupados
    Private Sub ChangeColors()
        For index = 0 To ellipses.Count - 1
            ellipses(index).Fill = Brushes.WhiteSmoke
        Next
    End Sub
    Private Sub Tablero_Initialized(sender As Object, e As EventArgs) Handles zone.Initialized '<- método que carga el tablero al iniciar el juego.
        Reinicio()
    End Sub

    Private Sub btnReset_Click(sender As Object, e As RoutedEventArgs) Handles btnReset.Click  '<- Funcionalidad del boton (reset)
        Reinicio()
        MsgBox("RE-INICIANDO ", MsgBoxStyle.Information)
        Btn1.IsEnabled = True
        Btn2.IsEnabled = True
        txtPlayer1.Clear() : txtPlayer4.Clear()
        txtPlayer2.Clear() : txtPlayer5.Clear()
        txtPlayer3.Clear() : txtPlayer6.Clear()
    End Sub

    Private Sub Reinicio() ' <- funcionalidad del boton Reset.
        zone.Children.Clear()
        LoadBoard()
    End Sub
End Class